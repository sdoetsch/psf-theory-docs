# PSF Theory technical documentation

[**Read the documentation here**](https://psftheory.readthedocs.io/en/latest/)

Welcome to the technical documentation of the MPIA PSF Theory group.
This documentation is meant as an introduction for new users on connecting
to the Linux clusters used for simulations.

It will go into details on how to connect to the clusters, job scheduling,
data analysis with python, etc.

## Contributing
If you want to add or correct something, feel free to either create a Pull Request
on the [repository](https://gitlab.mpcdf.mpg.de/sdoetsch/psf-theory-docs)
or to contact me directly (via doetsch@mpia.de).

This is meant as a group resource, so your input is very welcome, especially
regarding other simulation codes.

The documentation is written in [Markdown](https://daringfireball.net/projects/markdown/)
and rendered to HTML using [MkDocs](https://www.mkdocs.org/).
This makes it very easy to write documentation.
For more information consult the [MkDocs User Guide](https://www.mkdocs.org/user-guide/writing-your-docs/)
