# File systems on MPCDF clusters
On the MPCDF clusters every user has usually access to a few directories

## Home directory
Each users home directory at `/u/<username>` is located on a GPFS based filesystem with an enforced quota.
It is shared from all nodes of the cluster in use (but not between the clusters).

The quota is dependent on the cluster:

- ISAAC: 1 TB
- COBRA: 2.5 TB / 2e6 files
- RAVEN: 2.5 TB / 2e6 files
- DRACO: 1.5 TB

The current quota can be checked with:

- ISAAC: `/usr/lpp/mmfs/bin/mmlsquota`
- COBRA: `/usr/lpp/mmfs/bin/mmlsquota cobra_u`
- RAVEN: `/usr/lpp/mmfs/bin/mmlsquota raven_u`
- DRACO: `/usr/lpp/mmfs/bin/mmlsquota draco_u`

## PTMP scratch directory
If more space is needed a larger scratch filesystem is provided, which is also accessible on all nodes of a cluster.
This is handled differently on the large MPCDF wide clusters (COBRA, RAVEN, DRACO) and the institute specific clusters (e.g. ISAAC).

Be aware that there are no backups of PTMP, and files can be deleted if not accessed for 12 weeks (at least on some clusters).

### PTMP on COBRA, RAVEN, DRACO
Each users scratch directory is located at `/ptmp/<username>`.

There is no enforced quota, but files which haven't been accessed for more than 12 weeks will be deleted automatically.
So if files are needed for longer, they should be moved to the home directory, or to the archive system.

### PTMP on ISAAC
The scratch directory on ISAAC is located at `/isaac/ptmp/<department>/<username>` is currently not created automatically.
The creation has to be requested at the helpdesk (Citation needed).

On ISAAC PTMP has an enforced quota of 5 TB, but old files will not be deleted.


## Shared filesystems between clusters
On the large clusters (COBRA, RAVEN, DRACO) the GPFS filesystems can be directly accessed between the clusters:

- `/cobra/u` & `/cobra/ptmp`
- `/draco/u` & `/draco/ptmp`
- `/raven/u` & `/raven/ptmp`

As well as the archive system, which can be accessed at `/r`.

## MPCDF archive system
For long term storage the MPCDF provides an archive system, which is a large filesystem backed by a tape library at `archive.mpcdf.mpg.de`.

On the large clusters COBRA, RAVEN, DRACO it can be directly accessed at `/r`, for ISAAC files have to be transferred via SSH.

Filessizes on the archive system should be in the range of 1GB to 1TB, as files will be moved to tape if they are not accessed for a longer time.
Therefore if a lot of small files should be archived, they should be packed into an archive file (like tar or zip).

If a file on is only available on tape and is accessed, the library will load the data back onto disk, but this can take a while.
To check where a file currently resides use the command `ghi_ls`.
If many files are needed they can be staged for retrieval from tape with the `ghi_stage` command, this allows for more efficient tape reading.

[More information on how to use the archive system](https://www.mpcdf.mpg.de/services/data/backup-archive/archives)


## Sharing files/directories between users

Directories can be made available to other users using ACL (access control lists).

[More information can be found on the MPCDF site](https://www.mpcdf.mpg.de/services/computing/hydra/sharing-files-in-gpfs)

### Short command cheat sheet
#### Granting access
Granting recursive read access to  `/u/<username>/<shared_directory>`
```
setfacl -R -m user:john:rx /u/<username>/<somedir>/<shared_directory>
# allow directory read access for all parent directories
setfacl -m user:john:rx /u/<username> /u/<username> /u/<username>/<somedir>
```

## Access to distributed filesystem AFS
[More information at MPCDF](https://www.mpcdf.mpg.de/services/data/store)

## Links to Filesystem pages at MPCDF
- [COBRA](https://www.mpcdf.mpg.de/services/computing/cobra/filesystems)
- [RAVEN](https://www.mpcdf.mpg.de/services/computing/raven/filesystems)
- [DRACO](https://www.mpcdf.mpg.de/services/computing/draco/filesystems)
- [ISAAC](https://www.mpcdf.mpg.de/services/computing/linux/Astronomy)