# File transfer to the clusters
File transfer from and to the clusters is very important.
Usually the simulation configurations have to be send to the cluster,
and plots and other simulation results need to be transferred back to the local computer.

Sometimes it is even useful to mount the remote cluster filesystem on the local computer.
In this chapter three methods for file transfer are presented:

- `scp` for simple file transfers
- `rsync` for more bigger file transfers or directory  mirroring
- `sshfs` for mounting the remote filesystem on the local machine

All of these tools use a SSH connection, so the SSH configuration must be set up
correctly to allow for a direct connection to the remote machine.
This is explained in ["Connection to the clusters"](ssh.md).

## `scp` - Simple file transfer over SSH
To simply transfer a file or directory, which doesn't contain too many files
or big files, the unix tool `scp` is the easiest tool.
The commands are very similar to unix copy tool `cp`, only that the remote path
has to be prefixed with `host:`.

For example to transfer the file `foo.txt` from the current directory on the local machine
to the directory `~/bar/` on ISAAC, the command would be
```
scp foo.txt isaac:bar/
```

To download the file into the current directory it would be:
```
scp isaac:bar/foo.txt .
```

The source/target `isaac:` directly points to the remote home directory.

To copy a directory `bar`, the recursive flag `-r` is needed (like with `cp`):
```
ssh -r isaac:bar/ .
```

For more details consult the [man page of scp](https://linux.die.net/man/1/scp) (`man scp`).

## `rsync` - More complex file transfer
The capabilities of `scp` are limited, especially with large files and directories.
`rsync` uses a more efficient algorithm to allow for folder mirroring, such that
if the source directory or file has changed since the last copy, only the differences
have to be transferred.
This allows for resumable transfer, which is especially useful for large files.

The basic syntax is the same as for `scp`:
```
rsync foo.txt isaac:bar/
rsync isaac:bar/foo.txt .
ssh -r isaac:bar/ .
```

But it allows for much more complex transfers.
For example the archive mode:
```
rsync -a foo/ isaac:
```
In the archive mode (flag `-a`) the directory will be copied to represent a
full mirror (including permissions, modification times, symlinks, etc).

`rsync` also allows for compression while transfering with the flag `-z`.

`rsync` can show different progress indicators:

- `-P`, `--progress`: show per file progress and time estimation
- `--info=progress2`: show total progress and time estimation

For more details and information about all of `rsync`s capabilities consult
the [man page of rsync](https://linux.die.net/man/1/rsync)


## `sshfs` - Mounting remote filesystem over SSH
If direct access to the data on the cluster with software running
on the local machine (for example to show a video player) is needed,
SSHFS can mount a remote directory on the local machine.

Please be aware that the latency and speed of the mount is relatively slow.
While video playback or file editing works fine, I would not edit very large files
or access many small files.
Such operations should be performed either directly on the cluster, or after
download with `rsync`.

**Note:** SSHFS only needs to be installed on the local machine. No installation
is necessary on the cluster.

### Installation on Linux
To install SSHFS the [userspace filesystem library FUSE](https://github.com/libfuse/libfuse)
has to be installed.
On Linux systems this should be the default, otherwise it needs to be installed
from the package manager.
Then [SSHFS](https://github.com/libfuse/sshfs) can also be installed
from the package manager (sometimes it is also installed by default).

### Installation on MacOS
As on Linux SSHFS needs FUSE to be installed.
FUSE and SSHFS can both be downloaded and installed from [FUSE for macOS](https://osxfuse.github.io/).

### Usage
To mount a remote filesystem, an empty directory has to be created as the mountpoint
(e.g. `mkdir mountpoint`).
Then a remote directory (in this case the home directory) can be mounted into the local filesystem:
```
sshfs isaac: mountpoint
```
Mount a specific directory:
```
sshfs isaac:bar mountpoint
```

Now the remote directory can be accessed in `mountpoint` as it were directly
connected to the computer.

To unmount the directory, use either
```
umount mountpoint
```
or
```
fusermount -u mountpoint
```

Per default SSFHS doesn't handle disconnects very well, and will block all access
to the mountpoint directory. If this happens, just kill the sshfs process and
unmount the mounpoint.

To make SSHFS more resistant against disconnects, use the flags:
```
sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3 isaac1: mountpoint
```
With this SSHFS will check every 15 seconds if the server is still responding.
If the server doesn't respond within 1 minute, SSHFS will try to reconnect.
If the connection doesn't come back, the mountpoint has to unmounted with `fusermount -u mountpoint`.

To always use the sshfs reconnect settings, create an alias in your `~/.bashrc` file:
```
alias sshfs="sshfs -o reconnect,ServerAliveInterval=15,ServerAliveCountMax=3"
```

An interrupted connection can lead to loss of data, which was not written to the server.
