# Introduction
For large scale computations the MPIA has access to multiple compute clusters.

A compute cluster consists of a number of compute nodes with a certain amount of
CPU cores and memory (RAM).
Depending on the cluster nodes may also have connected GPUs.

All nodes have access to the same filesystem, and have a high speed interlink between them.

Users don't have access to the compute nodes themself, only to the so called "login nodes".
From the login node a scheduling system is used to submit compute jobs to the cluster.
The scheduling system then allocates the number of requested resources
(nodes,cores,memory,GPU) as soon as the queue allows and starts the job.

The ["Max Planck Computing & Data Facility" (MPCDF)](https://www.mpcdf.mpg.de/) provides
clusters in its datacenter for use in the MPG.

The MPIA has its own cluster at the MPCDF called ["ISAAC"](https://www.mpcdf.mpg.de/services/computing/linux/Astronomy).
It consists of 84 nodes with 40 cores each.

Additionally the MPIA has access to the HPC systems [Cobra](https://www.mpcdf.mpg.de/services/computing/cobra)
and [Draco](https://www.mpcdf.mpg.de/services/computing/draco).
These are much larger (Cobra: 3424 nodes, Draco: 944 nodes), and also provide GPUs on some nodes.
Cobra and Draco are in use by the whole Max-Planck-Gesellschaft, so there are many more people using it.

The MPIA also has a cluster hosted inhouse called "Bachelor".

# Getting Access
To get access to the MPCDF clusters go to the [registration form](https://www.mpcdf.mpg.de/userspace/forms/onlineregistrationform)
on the MPCDF website and click on "MPI für Astronomie".
Then select the institute representative for your group, fill out the form and select the desired systems.
The institute representative has to approve the access.
As soon as the access is activated, you should get an email.

# MPCDF technical documentation
The MPCDF provides its own documentation: [MPCDF technical documentation](https://docs.mpcdf.mpg.de/)
