# Module Manager

Because the cluster is used by many people with very different simulation codes
a large collection of libraries in different versions is needed.
To prevent any conflict between software/library versions most of the
libraries have to be loaded with the module manager before usage.

The module manager can be called with
```
module
```
This will display a short help text with all subcommands.

To show all available packages use
```
module avail
```
This will output a long list of grouped modules.
In the group `compilers` for example are a range of gcc and intel compiler versions.
In the other groups tools like `cmake` or `git` can be found, as well as
libraries like `hdf5`, `gsl` or `mkl`, often in multiple versions.

To only list the versions of specific package run
```
module avail gcc
```
To display module specific help use:
```
module help gcc
```


To load a module run (like gcc)
```
module load gcc
```
This will load the default version.
The version can also be specified:
```
module load gcc/7.2
```

Modules can be unloaded with
```
module unload gcc
```
Or if you want to unload all loaded modules
```
module purge
```
