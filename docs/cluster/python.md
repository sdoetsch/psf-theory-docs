# Using Python on the Cluster

While Python is provided on the cluster through the module manager,
there is no control over the environment.
Therefore it is often useful to use a custom Python installation through the
package manager conda.


## Module manager Python

The module manager on the clusters offers a variety of Anaconda version with
different Python versions.
These can be loaded as usual:
```
module avail anaconda
module load anaconda/3
```

This is fine if the newest Python version meet your requirements, and all needed
packages are installed.
New packages cannot be installed (through conda or pip) in the root environment because this would affect
all users on the cluster.

Therefore a new local environment has to be created to allow package installations
Using the module manager anaconda also allows you to create custom environments:
!!TODO

## Custom `conda` Python installation
To get full control of your conda and Python installation a conda installation
in the user home directory is the best way.

This can be achieved with the conda distribution "miniconda".
To install it, go to the [Miniconda download page](https://docs.conda.io/en/latest/miniconda.html)
and copy the link to the latest linux version.
Download it to the cluster with:
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
```
And run it with
```
bash Miniconda3-latest-x86_64.sh
```
This will lead through the installation. In the last step the installer will ask
if miniconda should be run and put into your `.bashrc`.
With this the default environment of the miniconda installation will be automatically
loaded when logging in.

To use this Python installation in SLURM jobs, you have to load the `.bashrc`
in the job script (with `source ~/.bashrc`).

Now conda can be used as usual, for more information consult the
[documentation](https://docs.conda.io/projects/conda/en/latest/index.html)


## Jupyter Notebook/Lab on the cluster
To analyze and visualize simulation data it is very useful to use Jupyter Notebooks.
The newest interface for Jupyter Notebooks is [JupyterLab](https://github.com/jupyterlab/jupyterlab),
which has much more features. All users should switch to Jupyterlab, the notebook
format did not change, so all previous notebooks can be opened.

Jupyter servers allow per default only connections from the local machine,
and the clusters are behind the gateway server.
Therefore we need a SSH tunnel to connect to our Jupyter server.

To set this up first install Jupyterlab as usual:
```
conda install jupyterlab
```
Then we need to configure the server to

- use a custom port
- don't open a browser when launched

While this could be set in the commandline with flags when starting JupyterLab,
it is much more comfortable to set it in the configuration file.

To set these options, open the file `~/.jupyter/jupyter_notebook_config.py`
and set the options:
```
c.NotebookApp.open_browser = False
c.NotebookApp.port = 7512
```
The port should be chosen randomly in the range [1024, 49151].
The chosen port should differ from all other users, otherwise only one can run
Jupyter at a time.

If you want to see all options, you can generate the default configuration file with:
```
jupyter lab --generate-config
```
to `~/.jupyter/jupyter_notebook_config.py`. There you can then manually set the
above settings.

It is possible to set a password at this stage, but it is easier to do using
the web interface.

Now that the Jupyter server is configured, the SSH tunnel has to be created.
This will build on the SSH configuration from the chapter [Connecting to the Cluster](ssh.md).

To add a tunnel to a cluster, just add a new Host block for the tunnel:
```
Host isaac*-jupyter
    LocalForward 9000 localhost:7512
```
This will create a SSH tunnel connecting port 9000 on the local computer to
port 7512 on the cluster login node.
The port on the local machine can also be chosen freely, 9000 is simple to remember.
If the tunnel is configured to multiple clusters, the port on the cluster can stay the same.
But if both tunnels should function at the same time, the local port has to be changed
(e.g. counting up from 9000).

Now a connection with a tunnel can be initiated:
```
ssh isaac1-jupyter
```
Now we can start the Jupyter server on the cluster:
```
jupyter lab
```
Jupyter Lab should now be accessible on [http://localhost:9000](http://localhost:9000).
It will ask for a password or token. The token can be copied from the terminal.
To avoid having to copy the token every time Jupyter Lab is launched a password
can be setup on the login page using the token.

JupyterLab can be extended with many plugins,
[consult the documentation for more information](https://jupyterlab.readthedocs.io/en/stable/user/extensions.html).

If you want to have a long running Jupyter instance, you can e.g. use screen to
have a detachable server.
Example command to start server at current location in detached `screen`-session:
```
screen -S jupyter -dm jupyter lab
```
This can be saved in a alias.
