# Slurm Workload Manager

To distribute the workload of many user jobs on the compute nodes the
[SLURM Workload Manager](https://slurm.schedmd.com/) is used.

To understand the basic operating principle of SLURM,
read the [SLURM Quick Start User Guide](https://slurm.schedmd.com/quickstart.html).

Information on the partition setup on the MPCDF clusters as well as sample job scripts
can be found on the MPCDF website:

- [COBRA](https://www.mpcdf.mpg.de/services/computing/cobra/batch-system)
- [DRACO](https://www.mpcdf.mpg.de/services/computing/draco/batch-system)
- [ISAAC](https://www.mpcdf.mpg.de/services/computing/linux/Astronomy)

SLURM takes in a number of parameters in choosing when to start each job.
One of these is the maximal runtime.
If it is known, that the job will be shorter than a certain time,
it can be useful to set the wall clock limit to it with
```
#SBATCH --time=HH:MM:SS
```
in the sbatch script.
