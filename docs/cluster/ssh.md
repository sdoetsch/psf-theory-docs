# Connecting to the Clusters

The Linux clusters are usually accessed via SSH.
On unix-like systems (MacOS, Linux-distributions) SSH is provided with the operating system.

On Windows it is possible either through a SSH client like [PuTTY](https://putty.org/)
or using the ["Linux Subsystem for Windows" (WSL)](https://docs.microsoft.com/en-us/windows/wsl/install-win10).
The latter only works on Windows 10, but is much more powerful.
WSL gives you access to a native Bash-Shell (or any other Linux shell).
This is very useful for working with the cluster.

Before connecting to the cluster you should make yourself confortable in the
Linux commandline. ([Quick Tutorial by the Ubuntu foundation](https://tutorials.ubuntu.com/tutorial/command-line-for-beginners))

## Topography of the MPCDF network
Each cluster has a small number of login nodes.
Users can only connect to the login nodes, and schedule jobs from there,
which then run on the compute nodes.

The login nodes are not meant to be used for any heavy computation, it should only
be used for organization, compilation and non-heavy analysis.
Which of the login nodes is used is arbitrary, but the users should be evenly distributed.

As a protection layer between the login nodes and the open internet is pair of gate nodes.
A connection to a login nodes always has to go through the gate.


## Authentification
The MPCDF uses the Kerberos for authentification.
Therefore SSH public key authentification is disabled.
When connecting to a cluster through the gate, one always has to enter their password twice
(once for the gate, once for the login node).
Passwordless authentification is possible by obtaining a Kerberos ticket.
Such a ticket then allows access to the clusters without a password for 24h.

To obtain a Kerberos ticket, the `krb5` package needs to be installed.
A ticket can then be obtained using
```
kinit username@IPP-GARCHING.MPG.DE
```
The command `klist` provides a list of current tickets.

To login without a password the SSH configuration needs to be adjusted.

## SSH configuration
While it is possible to first connect to `gate`, and then use another SSH connection
to connect to the login node of the cluster, it is much faster to set up
a SSH tunnel through `gate` to allow for direct connections.
This is also useful for file transfer using `scp`, `rsync` or `sshfs`.

While the SSH configuration could be done with command line arguments,
it is more clear using the SSH configuration file `~/.ssh/config`.

Here is a sample configuration file for connecting to isaac.
Connecting to COBRA or DRACO is equivalent.
`~/.ssh/config`
```
Host mpg-gate
    Hostname gatezero.rzg.mpg.de

Host isaac
    Hostname isaac1.bc.rzg.mpg.de
    ProxyJump mpg-gate

Match host "*.rzg.mpg.de,*.mpcdf.mpg.de"
    User username
    GSSAPIAuthentication yes
    GSSAPIDelegateCredentials yes
```
Explanation:

 - `Match`: set username and enable GSSAPI (for Kerberos authentification) for all MPCDF domains
 - `Host mpg-gate`: setup hostname of gate
 - `Host isaac`: setup hostname of isaac login node and the tunnel through the proxy `gate`

With this configuration the login node 1 of ISAAC can be reached with:
```
ssh isaac
```

Here is a full SSH configuration for the MPCDF clusters.
Only the username has to be filled in the last block:
[**MPCDF SSH config**](mpcdf_sshconfig.txt)

This configuration gives access to the hosts:

 - `isaac`, `isaac1`: ISAAC login node 1
 - `cobra`: COBRA login node 1 or 2 (random)
   - `cobra1,cobra2`: COBRA login nodes 1 or 2 (decided by user)
   - `cobra#i` with # beeing the number 3-6 for the interactive nodes.
 - `draco`: DRACO login nodes 1 or 2 (random)
   - `draco1`, `draco2`: DRACO login nodes 1 or 2 (decided by user)
 - `raven`: RAVEN login node
 - `mpg-archive`: MPCDF archive server

Before using the interactive cobra nodes, please read the [notes on the MPCDF website](https://www.mpcdf.mpg.de/services/computing/cobra/access-to-the-HPC-system).
Note that the configuration uses a `Match`-block to setup the tunneling through the gate.
There is a bit of care needed for editing the configuration.

To use the provided configuration, download the file and place it in `~/.ssh`.
Change the permissions to private with `chmod mpcdf_sshconfig.txt 600`.
Add the following to your exisiting SSH config (`~/.ssh/config`) (if it doesn't exist, create it)
```
Include mpcdf_sshconfig.txt
```

Now you should be able to connect to the clusters using
```
ssh isaac
```
or any other host mentioned in the list above.

# X11 forwarding (opening graphical programs on the cluster)
If X11 forwarding is needed to open graphical programs on the login nodes
(such as IDL/matplotlib plotting windows, etc.) uncomment lines 11-13 in the
provided SSH configuration file.

This sets
```
ForwardX11 yes
ForwardX11Trusted yes
```

Please be aware of the security implications when enabling these options.

For Python plotting with `matplotlib` using a JupyterLab instance on the cluster
is much more versatile, go to the Python chapter in this documentation to find
the needed configuration.
