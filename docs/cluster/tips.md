# General cluster usage tips

Here are just some resources useful for everyone not that familiar with
a bash-like commandlines.

## Bash
- [Bash introduction](https://programminghistorian.org/en/lessons/intro-to-bash)
- [Bash configuration, aliases, functions](https://www.maketecheasier.com/what-is-bashrc/)
- [Bash scripting](https://linuxconfig.org/bash-scripting-tutorial)

## Paths
File path abbreviations can be confusing. Therefore a very short overview:

- `.`: current directory
- `..`: parent directory
- `~`: home directory

## Text editors
(all mentioned editors are installed on the cluster)

- `nano`: [GNU nano](https://www.nano-editor.org/docs.php): Easy to use editor,
    mostly self-explanatory
- `vim`: [Vim](https://www.vim.org/): Much more powerful, has a bit of learning curve
- `emacs`: [GNU Emacs](https://www.gnu.org/software/emacs/documentation.html):
Also very powerful editor, also has a bit of a learning curve.

## Persistent terminal sessions - `screen`
If a SSH connection is disconnected (intentionally or not) all foreground applications
will be closed, and potentially work lost.
Multiple terminal windows using one SSH connection is also very useful.

The terminal multiplexer [GNU screen](https://www.gnu.org/software/screen/)
is solution to this problem.
With this your terminal session can persist over multiple SSH sessions.

[Introductory tutorial to GNU screen](https://opensource.com/article/17/3/introduction-gnu-screen)

The default GNU screen configuration doesn't show any status bar of open windows
or any indication that a screen session is running (after the welcome screen).

Here is a [sample screen config](screenrc.txt) which provides a status bar.
The screen configuration needs to be at `~/.screenrc`.

(__Note__: The terminal multiplexer `tmux` has be become also very popular.
However it is not installed on the cluster.)

## Version control - Git
Tracking source code changes is a standard tool in Computer Science,
and should be in Physics.
The tool of choice is [Git](https://git-scm.com/).

The [documentation](https://git-scm.com/doc) gives a good introduction.

On the clusters Git has to be loaded with the module manager (`module load git`).

The MPCDF offers it's own Gitlab instance.
To activate your Gitlab user account go to the
[MPCDF collaboration page](https://www.mpcdf.mpg.de/services/data/collaborate)
and follow the instructions.

To use git with Jupyter Notebooks the use of the [nbdime](https://github.com/jupyter/nbdime)
diff- and mergetool is recommended.
nbdime also provides a [Jupyterlab extension](https://nbdime.readthedocs.io/en/latest/extensions.html).
